<?php

namespace App\Http\Middleware;

use Closure;
use auth;
class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth::user()->isSuperAdmin()){
            return $next($request);    
        }else if(auth::user()->isAdmin()){
            if($request->path()=='superAdmin'){
                abort(403);
            }
            return $next($request);    
        }else{
            abort(403);
        }
    }
}
